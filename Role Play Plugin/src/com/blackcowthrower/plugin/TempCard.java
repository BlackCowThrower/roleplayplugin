package com.blackcowthrower.plugin;

import java.util.UUID;

import org.bukkit.Bukkit;

public class TempCard
{
	private UUID uuid;
	private String name, bio, bio2;
	private int age;
	private String race;

	public TempCard(UUID uuid)
	{
		this.uuid = uuid;
	}
	
	public UUID getUUID()
	{
		return uuid;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public void setRace(String race)
	{
		this.race = race;
	}

	public void setBio(String bio)
	{
		this.bio = bio;
	}

	public void setBio2(String bio2)
	{
		this.bio2 = bio2;
	}
	
	public String getName()
	{
		return name;
	}

	public int getAge()
	{
		return age;
	}

	public String getRace()
	{
		return race;
	}

	public String getBio()
	{
		return bio;
	}

	public String getBio2()
	{
		return bio2;
	}
	
	public Card finalise()
	{
		if((!name.isEmpty()) && (age != 0) && (!race.toString().isEmpty()) && (!bio.isEmpty()))
		{
			Card c = new Card(Bukkit.getPlayer(uuid), name, age, race, bio, bio2);
			Main.tempCards.remove(this);
			return c;
		}
		return null;
	}
	
	public static TempCard checkForCard(UUID uuid)
	{
		for(TempCard tc: Main.tempCards)
		{
			if(tc.getUUID().equals(uuid))
				return tc;
		}
		return null;
	}
	
	public static void updateTempCard(TempCard newCard)
	{
		TempCard card = checkForCard(newCard.getUUID());
		Main.tempCards.remove(card);
		Main.tempCards.add(newCard);
	}
}
