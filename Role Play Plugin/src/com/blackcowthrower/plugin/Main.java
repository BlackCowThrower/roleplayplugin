package com.blackcowthrower.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.blackcowthrower.plugin.events.onAsyncPlayerChat;
import com.blackcowthrower.plugin.events.onPlayerCommandPreprocess;
import com.blackcowthrower.plugin.events.onPlayerInteractEntity;
import com.blackcowthrower.plugin.events.onPlayerJoin;
import com.blackcowthrower.plugin.events.onPlayerQuit;

public class Main extends JavaPlugin
{
	private File dataFolder;
	private static Main plugin;
	private static MySQL mysql;
	private static String prefix;
	private static List<Card> cards;
	protected static ArrayList<TempCard> tempCards;
	private static Config config;

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable()
	{
		plugin = this;
		dataFolder = plugin.getDataFolder();

		Bukkit.getPluginManager().registerEvents(new onAsyncPlayerChat(),
				plugin);
		Bukkit.getPluginManager().registerEvents(
				new onPlayerCommandPreprocess(), plugin);
		Bukkit.getPluginManager().registerEvents(new onPlayerInteractEntity(),
				plugin);
		Bukkit.getPluginManager().registerEvents(new onPlayerJoin(), plugin);
		Bukkit.getPluginManager().registerEvents(new onPlayerQuit(), plugin);

		StringBuilder sb = new StringBuilder().append(ChatColor.BLACK)
				.append("[ ").append(ChatColor.DARK_GREEN).append("Chracter ")
				.append(ChatColor.BLACK).append("] ").append(ChatColor.WHITE);
		prefix = sb.toString();

		if (!dataFolder.exists())
			dataFolder.mkdirs();
		saveDefaultConfig();

		config = new Config(plugin);

		cards = new ArrayList<Card>();
		tempCards = new ArrayList<TempCard>();

		mysql = new MySQL();
		mysql.openConnection();
		if (Bukkit.getOnlinePlayers().length > 0)
		{
			for (Player player : Bukkit.getOnlinePlayers())
			{
				UUID uuid = player.getUniqueId();
				if (mysql.playerDataContainsPlayer(uuid))
				{
					Card c = mysql.getCard(uuid);
					if (c != null)
						cards.add(c);
				}
			}
		}
		CommandExecutor cmdEx = new CMDExecutor();
		getCommand("character").setExecutor(cmdEx);
		getCommand("l").setExecutor(cmdEx);
		getCommand("id").setExecutor(cmdEx);
		getCommand("s").setExecutor(cmdEx);
		getCommand("b").setExecutor(cmdEx);
		getCommand("me").setExecutor(cmdEx);
		getCommand("dice").setExecutor(cmdEx);
		getCommand("w").setExecutor(cmdEx);
		// TODO: Hook PEX & TabEdditor

		// TODO: Schedule level increase repeater
	}

	@Override
	public void onDisable()
	{
		mysql.closeConnection();
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command,
			String alias, String[] args)
	{
		if (alias.equalsIgnoreCase("character"))
		{
			if (args.length > 0)
			{
				if (args[0].equalsIgnoreCase("race"))
				{
					return config.races;
				}
			}
		}
		return null;
	}

	public static Main getPlugin()
	{
		return plugin;
	}

	public static String getPrefix()
	{
		return prefix;
	}

	public static MySQL getMySQLConnection()
	{
		return mysql;
	}

	public static List<Card> getCardList()
	{
		return cards;
	}

	public static void addCardToList(Card card)
	{
		cards.add(card);
	}

	public static void removeCardFromList(Card card)
	{
		cards.remove(card);
	}

	public static boolean checkCardExists(UUID uuid)
	{
		for (Card c : cards)
		{
			if (c.getUUID().equals(uuid))
				return true;
		}
		return false;
	}

	public static boolean checkCardExists(String RolePlayName)
	{
		for (Card c : cards)
		{
			if (c.getRPName().equalsIgnoreCase(RolePlayName))
				return true;
		}
		return false;
	}

	public static Card getCard(UUID uuid)
	{
		for (Card c : cards)
		{
			if (c.getUUID().equals(uuid))
				return c;
		}
		return null;
	}

	public static Card getCard(String RolePlayName)
	{
		for (Card c : cards)
		{
			if (c.getRPName().equalsIgnoreCase(RolePlayName))
				return c;
		}
		return null;
	}

	public static void setCards(List<Card> newCards)
	{
		cards.clear();
		cards.addAll(cards);
	}

	public static Config getPluginConfig()
	{
		return config;
	}

}
