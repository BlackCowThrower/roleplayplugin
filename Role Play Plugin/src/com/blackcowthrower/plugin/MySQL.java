package com.blackcowthrower.plugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;

public class MySQL
{
	private Connection connection;

	public synchronized void openConnection()
	{
		String host = Main.getPluginConfig().host;
		int port = Main.getPluginConfig().port;
		String database = Main.getPluginConfig().database;
		String user = Main.getPluginConfig().username;
		String pass = Main.getPluginConfig().password;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://" + host
					+ ":" + port + "/" + database, user, pass);
		} catch (Exception e)
		{
			e.printStackTrace();
			Bukkit.getPluginManager().disablePlugin(Main.getPlugin());
		}
	}

	public synchronized void closeConnection()
	{
		try
		{
			if (this.connection != null && !(this.connection.isClosed()))
				connection.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public synchronized boolean playerDataContainsPlayer(UUID uuid)
	{
		try
		{
			PreparedStatement sql = connection
					.prepareStatement("SELECT * FROM `Users` WHERE `UUID` = ?;");
			sql.setString(1, uuid.toString());
			ResultSet rs = sql.executeQuery();

			boolean containsPlayer = rs.next();

			sql.close();
			rs.close();

			return containsPlayer;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public Object getData(UUID uuid, String column)
	{
		try
		{
			if (playerDataContainsPlayer(uuid))
			{
				PreparedStatement sql = connection
						.prepareStatement("SELECT * FROM `Users` WHERE `UUID` = ?;");
				sql.setString(1, uuid.toString());
				ResultSet rs = sql.executeQuery();
				rs.next();
				Object toBeReturned = rs.getObject(column);

				sql.close();
				rs.close();

				return toBeReturned;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public List<Object> getColumn(String column)
	{
		try
		{
			PreparedStatement sql = connection
					.prepareStatement("SELECT * FROM `Users`;");
			ResultSet rs = sql.executeQuery();
			List<Object> values = new ArrayList<Object>();
			while (rs.next())
			{
				Object toBeReturned = rs.getObject(column);
				values.add(toBeReturned);
			}

			sql.close();
			rs.close();

			return values;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void createSQLCard(Card card)
	{
		try
		{
			PreparedStatement sql = connection
					.prepareStatement("INSERT INTO  `Users` (  `UUID` ,  `MC_Name` ,  `RP_Name` ,  `Age` ,  `Race` ,  `Bio` ,  `Bio2` ,  `Level` ,  `Minutes` ) VALUES (?,?,?,?,?,?,?,?,0);");
			sql.setString(1, card.getUUID().toString());
			sql.setString(2, card.getMCName());
			sql.setString(3, card.getRPName());
			sql.setInt(4, card.getAge());
			sql.setString(5, card.getRace().toString());
			sql.setString(6, card.getBio());
			sql.setString(7, card.getBio2());
			sql.setInt(8, card.getLevel());

			sql.executeUpdate();

			sql.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void deleteSQLCard(UUID uuid)
	{
		try
		{
			PreparedStatement sql = connection
					.prepareStatement("DELETE FROM `Users` WHERE `UUID` = ?;");
			sql.setString(1, uuid.toString());

			sql.executeUpdate();

			sql.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Card getCard(UUID uuid)
	{
		try
		{
			if (playerDataContainsPlayer(uuid))
			{
				PreparedStatement sql = connection
						.prepareStatement("SELECT * FROM `Users` WHERE `UUID` = ?;");
				sql.setString(1, uuid.toString());
				ResultSet rs = sql.executeQuery();
				rs.next();
				String rpname = rs.getString("RP_Name");
				int age = (int) rs.getInt("Age");
				String race = (String) rs.getString("Race");
				String bio = (String) rs.getString("Bio");
				String bio2 = (String) rs.getString("Bio2");
				int level = (int) rs.getInt("Level");
				Card c = new Card(Bukkit.getPlayer(uuid), rpname, age,
						race, bio, bio2);
				c.setLevel(level);
				sql.close();
				rs.close();

				return c;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void updateObject(UUID uuid, String column, Object newValue)
	{
		try
		{
			if (playerDataContainsPlayer(uuid))
			{
				PreparedStatement sql = connection
						.prepareStatement("UPDATE `Users` SET `" + column
								+ "` = ? WHERE `UUID` = ?;");
				sql.setObject(1, newValue);
				sql.setString(2, uuid.toString());

				sql.executeUpdate();

				sql.close();
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public Connection getConnection()
	{
		return connection;
	}
}
