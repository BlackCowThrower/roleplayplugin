package com.blackcowthrower.plugin.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Cooldown;
import com.blackcowthrower.plugin.Main;

public class onPlayerInteractEntity implements Listener
{
	@EventHandler
	public void playerInteractEntity(PlayerInteractEntityEvent e)
	{
		if (!e.isCancelled())
		{
			Player player = e.getPlayer();
			if (e.getRightClicked() instanceof Player)
			{
				if (player.isSneaking())
				{
					Player clicked = (Player) e.getRightClicked();
					if (Main.checkCardExists(player.getUniqueId()))
					{
						if (Main.checkCardExists(clicked.getUniqueId()))
						{
							if (Cooldown.tryCooldown(player, player
									.getUniqueId().toString() + "Card",
									Main.getPluginConfig().cooldown * 1000))
							{
								Card.displayCard(player,
										Main.getCard(clicked.getUniqueId()));
							} else
							{
								player.sendMessage(new StringBuilder()
										.append(Main.getPrefix())
										.append(Cooldown.getCooldown(player,
												player.getUniqueId().toString()
														+ "Card") / 1000)
										.append(" second(s) to wait before you are able to perform that action again")
										.toString());
							}
						} else
						{
							player.sendMessage(new StringBuilder()
									.append(Main.getPrefix())
									.append("That player has no card")
									.toString());
						}
					} else
					{
						player.sendMessage(new StringBuilder()
								.append(Main.getPrefix())
								.append("You must create your own card before you can view others")
								.toString());
					}
				}
			}
		}
	}

}
