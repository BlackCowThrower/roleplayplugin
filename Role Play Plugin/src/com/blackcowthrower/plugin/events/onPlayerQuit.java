package com.blackcowthrower.plugin.events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.blackcowthrower.plugin.Main;

public class onPlayerQuit implements Listener
{
	@EventHandler
	public void playerQuit(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();

		e.setQuitMessage("");

		int qRadius = Main.getPluginConfig().join_leave_radius;

		for (Entity entity : e.getPlayer().getNearbyEntities(qRadius, qRadius,
				qRadius))
		{
			if (entity instanceof Player)
			{
				Player entity1 = (Player) entity;

				String msg = "%n left for another land";
				msg = msg.replaceAll("%p", player.getDisplayName())
						.replaceAll("%n", player.getName())
						.replaceAll("(&([a-f0-9]))", "\u00A7$2");

				entity1.sendMessage(msg);
			}
		}

//		TabEdititing.terminateTab(player);
//		Bukkit.getServer().getScheduler().runTaskAsynchronously(Character.plugin, new Runnable()
//		{
//			public void run()
//			{
//				for(Player p: Bukkit.getOnlinePlayers())
//					TabEdititing.updateTab(p);
//			}
//		});
	}

//	@EventHandler
//	public void playerKick(PlayerKickEvent e)
//	{
//		TabEdititing.terminateTab(e.getPlayer());
//		Bukkit.getServer().getScheduler().runTaskAsynchronously(Character.plugin, new Runnable()
//		{
//			public void run()
//			{
//				for(Player p: Bukkit.getOnlinePlayers())
//					TabEdititing.updateTab(p);
//			}
//		});
//	}
}
