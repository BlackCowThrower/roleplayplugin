package com.blackcowthrower.plugin.events;

import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.blackcowthrower.plugin.Cooldown;
import com.blackcowthrower.plugin.Main;

public class onAsyncPlayerChat implements Listener
{
	@EventHandler
	public static void asyncPlayerChat(AsyncPlayerChatEvent e)
	{
			if (e.isCancelled())
				return;

			Player player = e.getPlayer();
			if (Main.checkCardExists(player.getUniqueId()))
			{
				if (Cooldown.tryCooldown(player, "chat",
						Main.getPluginConfig().cooldown_chat * 1000))
				{
					int radius = Main.getPluginConfig().general_radius;
					e.setFormat("%1$s: %2$s");
					if (radius != 0)
					{
						String msg = ChatColor.GRAY + player.getDisplayName()
								+ ": " + ChatColor.WHITE + e.getMessage();
						String cMsg = player.getDisplayName() + ": "
								+ e.getMessage();

						int found = 0;
						for (Entity entity : player.getNearbyEntities(radius,
								radius, radius))
						{
							if (entity instanceof Player)
							{
								Player p = (Player) entity;

								if (Main.checkCardExists(p.getUniqueId()))
								{
									p.sendMessage(msg);
									found++;
								}
							}
						}
						player.sendMessage(msg);

						if (found == 0)
							player.sendMessage(ChatColor.GRAY
									+ "Your message fell upon deaf ears");

						Main.getPlugin().getLogger().log(Level.INFO, cMsg);

					}
				}else
				{
					player.sendMessage(new StringBuilder().append(Main.getPrefix())
							.append(ChatColor.RED)
							.append("You are still under cooldown").toString());
				}

			} 
			e.setCancelled(true);
	}
}
