package com.blackcowthrower.plugin.events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Main;

public class onPlayerJoin implements Listener
{
	@EventHandler
	public void playerJoin(PlayerJoinEvent e)
	{
		try
		{
			Player player = e.getPlayer();
			e.setJoinMessage("");
			if (Main.checkCardExists(player.getUniqueId()))
			{
				
				Card c = Main.getCard(player.getUniqueId());
				onJoinMessages(player, c);
				// TODO: Will not work because will not be in array, need to add
				// mysql statement to get player
				// TODO: Work on only online players card loaded basis. Do not
				// load all.
			}else if(Main.getMySQLConnection().playerDataContainsPlayer(player.getUniqueId()))
			{
				Card c = Main.getMySQLConnection().getCard(player.getUniqueId());
				Main.addCardToList(c);
				onJoinMessages(player, c);
			}
				
//			TODO: TabEditing + TagEditing.
//			TabEdititing.updateTab(player);
		} catch (Exception e1)
		{
			e1.printStackTrace();
		}
	}
	
	private void onJoinMessages(Player player, Card c)
	{
		String[] names = c.getRPName().split("_");
		player.setDisplayName(new StringBuilder().append(names[0])
				.append(" ").append(names[1]).toString());

		String join_msg = "Welcome %p";
		if (!join_msg.isEmpty())
		{
			join_msg = join_msg.replaceAll("%p",
					player.getDisplayName()).replaceAll("%n",
					player.getName());
			join_msg = ChatColor.translateAlternateColorCodes('&',
					join_msg);

			player.sendMessage(join_msg);
		}

		String join_msg2 = "";
		if (!join_msg2.isEmpty())
		{
			join_msg2 = join_msg2.replaceAll("%p",
					player.getDisplayName()).replaceAll("%n",
					player.getName());
			join_msg2 = ChatColor.translateAlternateColorCodes('&',
					join_msg2);

			player.sendMessage(join_msg2);
		}

		String join_msg3 = "";
		if (!join_msg3.isEmpty())
		{
			join_msg3 = join_msg3.replaceAll("%p",
					player.getDisplayName()).replaceAll("%n",
					player.getName());
			join_msg3 = ChatColor.translateAlternateColorCodes('&',
					join_msg3);

			player.sendMessage(join_msg3);
		}

		int jRadius = Main.getPluginConfig().join_leave_radius;
		String msg = "Your return was much anticipated %n";
		if (!msg.isEmpty())
		{
			msg = msg.replaceAll("%p", player.getDisplayName())
					.replaceAll("%n", player.getName());
			msg = ChatColor.translateAlternateColorCodes('&', msg);
			for (Entity entity : player.getNearbyEntities(jRadius,
					jRadius, jRadius))
			{
				if (entity instanceof Player)
				{
					Player pEntity = (Player) entity;

					pEntity.sendMessage(msg);
				}
			}
		}
	}
	
//	@EventHandler
	public void playerLoginEvent(PlayerLoginEvent e)
	{
//		TODO: TabEditing + TagEditing.
//		TabEdititing.initTab(e.getPlayer());
//		Bukkit.getServer().getScheduler()
//				.runTaskAsynchronously(Main.getPlugin(), new Runnable()
//				{
//					public void run()
//					{
//						for (Player p : Bukkit.getOnlinePlayers())
//							TabEdititing.updateTab(p);
//					}
//				});
	}
}
