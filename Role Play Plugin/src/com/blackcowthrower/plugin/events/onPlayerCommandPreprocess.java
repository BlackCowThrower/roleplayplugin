package com.blackcowthrower.plugin.events;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.blackcowthrower.plugin.Main;

public class onPlayerCommandPreprocess implements Listener
{
	@EventHandler
	public static void playerCommandPreprocess(PlayerCommandPreprocessEvent e)
	{
		if (!e.getPlayer().hasPermission("character.talk"))
			if (!Main.checkCardExists(e.getPlayer().getUniqueId()))
			{
				if (!e.getMessage().toLowerCase().startsWith("/help")
						&& !e.getMessage().toLowerCase()
								.startsWith("/character"))
				{
					e.getPlayer()
							.sendMessage(
									new StringBuilder()
											.append(ChatColor.RED)
											.append("You must create a character before you do that. /Character help")
											.toString());
					e.setCancelled(true);
				}
			}
	}
}
