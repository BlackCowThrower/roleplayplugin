package com.blackcowthrower.plugin;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Card
{
	private String MC_name, RP_name, bio, bio2;
	private UUID uuid;
	private String race;
	private int age, level;

	// TODO: Add Job

	public Card(Player player, String RP_name, int age, String race, String bio,
			String bio2)
	{
		this.RP_name = RP_name;
		this.MC_name = player.getName();
		this.uuid = player.getUniqueId();
		this.age = age;
		this.race = race;
		this.bio = bio;
		this.bio2 = bio2;
		this.level = 1;
	}

	public String getRPName()
	{
		return RP_name;
	}

	public void setRPName(String newName)
	{
		newName = RP_name;
	}

	public String getMCName()
	{
		return MC_name;
	}

	public void setMCName(String newName)
	{
		newName = MC_name;
	}

	public UUID getUUID()
	{
		return uuid;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int newAge)
	{
		newAge = age;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int newLevel)
	{
		newLevel = level;
	}

	public String getRace()
	{
		return race;
	}

	public void setRace(String newRace)
	{
		newRace = race;
	}

	public String getBio()
	{
		return bio;
	}

	public void setBio(String newBio)
	{
		newBio = bio;
	}

	public String getBio2()
	{
		return bio2;
	}

	public void setBio2(String newBio2)
	{
		newBio2 = bio2;
	}

	public static void displayCard(Player viewer, Card card)
	{
		int mins = (int) Main.getMySQLConnection().getData(card.getUUID(),
				"Minutes");
		mins = mins / 60;

		if (card.getBio().isEmpty())
			card.setBio("-");
		String[] names = card.getRPName().split("_");

		viewer.sendMessage(new StringBuilder().append(ChatColor.GREEN)
				.append("=====[").append(ChatColor.GOLD)
				.append("Character Cards").append(ChatColor.GREEN)
				.append("]=====").toString());
		viewer.sendMessage(new StringBuilder().append(ChatColor.BLUE)
				.append(ChatColor.BOLD).append("Name: ")
				.append(ChatColor.WHITE).append(names[0]).append(" ")
				.append(names[1]).toString());
		viewer.sendMessage(new StringBuilder().append(ChatColor.BLUE)
				.append(ChatColor.BOLD).append("Race: ")
				.append(ChatColor.WHITE).append(card.getRace()).toString());
		viewer.sendMessage(new StringBuilder().append(ChatColor.BLUE)
				.append(ChatColor.BOLD).append("Age: ").append(ChatColor.WHITE)
				.append(card.getAge()).toString());
		viewer.sendMessage(new StringBuilder().append(ChatColor.BLUE)
				.append(ChatColor.BOLD).append("Info: ")
				.append(ChatColor.WHITE).append(card.getBio()).toString());
		if (!card.getBio().isEmpty())
			viewer.sendMessage(new StringBuilder().append(ChatColor.WHITE)
					.append(card.getBio2()).toString());
		if (viewer.getUniqueId().equals(card.getUUID()))
		{
			viewer.sendMessage(new StringBuilder().append(ChatColor.GREEN)
					.append("------------------------").toString());
			viewer.sendMessage(new StringBuilder().append(ChatColor.BLUE)
					.append(ChatColor.BOLD).append("Level: ")
					.append(ChatColor.WHITE).append(card.getLevel())
					.append(" (" + mins + "/5)").toString());
			viewer.sendMessage(new StringBuilder().append(ChatColor.GREEN)
					.append("------------------------").toString());
		}
	}
}
