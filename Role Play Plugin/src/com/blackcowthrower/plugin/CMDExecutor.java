package com.blackcowthrower.plugin;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.commands.Dice;
import com.blackcowthrower.plugin.commands.Do;
import com.blackcowthrower.plugin.commands.ID;
import com.blackcowthrower.plugin.commands.Low;
import com.blackcowthrower.plugin.commands.Me;
import com.blackcowthrower.plugin.commands.OOC;
import com.blackcowthrower.plugin.commands.Shout;
import com.blackcowthrower.plugin.commands.Whisper;
import com.blackcowthrower.plugin.commands.character.Age;
import com.blackcowthrower.plugin.commands.character.Bio;
import com.blackcowthrower.plugin.commands.character.Bio2;
import com.blackcowthrower.plugin.commands.character.Character;
import com.blackcowthrower.plugin.commands.character.Confirm;
import com.blackcowthrower.plugin.commands.character.Delete;
import com.blackcowthrower.plugin.commands.character.Help;
import com.blackcowthrower.plugin.commands.character.Name;
import com.blackcowthrower.plugin.commands.character.Race;

public class CMDExecutor implements CommandExecutor
{
	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args)
	{

		if (!(sender instanceof Player))
		{
			sender.sendMessage(new StringBuilder().append(ChatColor.RED)
					.append("Command unavalible for non-players").toString());
			return true;
		}

		Player player = (Player) sender;

		if (label.equalsIgnoreCase("character"))
		{
			TempCard tc = TempCard.checkForCard(player.getUniqueId());
			if (tc == null)
			{
				tc = new TempCard(player.getUniqueId());
				Main.tempCards.add(tc);
			}
			if (args.length == 0)
			{
				new Character(sender, label, args);
			} else if (args[0].equalsIgnoreCase("name"))
			{
				Name name = new Name(sender, label, args);
				tc.setName(name.getName());
				TempCard.updateTempCard(tc);
			} else if (args[0].equalsIgnoreCase("race"))
			{
				Race race = new Race(sender, label, args);
				tc.setRace(race.getRace());
				TempCard.updateTempCard(tc);
			} else if (args[0].equalsIgnoreCase("age"))
			{
				Age age = new Age(sender, label, args);
				tc.setAge(age.getAge());
				TempCard.updateTempCard(tc);
			} else if (args[0].equalsIgnoreCase("bio"))
			{
				Bio bio = new Bio(sender, label, args);
				tc.setBio(bio.getBio());
				TempCard.updateTempCard(tc);
			} else if (args[0].equalsIgnoreCase("bio2"))
			{
				Bio2 bio2 = new Bio2(sender, label, args);
				tc.setBio2(bio2.getBio2());
				TempCard.updateTempCard(tc);
			} else if (args[0].equalsIgnoreCase("delete"))
			{
				new Delete(sender, label, args);
			} else if (args[0].equalsIgnoreCase("confirm"))
			{
				new Confirm(sender, label, args);
			} else if (args[0].equalsIgnoreCase("help"))
			{
				new Help(sender, label, args);
			} else
			{
				player.sendMessage(new StringBuilder().append(Main.getPrefix())
						.append(ChatColor.RED).append("Command unkown")
						.toString());
			}
		} else if (label.equalsIgnoreCase("l"))
		{
			new Low(sender, label, args);
		} else if (label.equalsIgnoreCase("id"))
		{
			new ID(sender, label, args);
		} else if (label.equalsIgnoreCase("s"))
		{
			new Shout(sender, label, args);
			// } else if (label.equalsIgnoreCase("channel"))
			// {
			// Kanal.sendKanal(player, args);
			// } else if (label.equalsIgnoreCase("channelhelp"))
			// {
			// KanalYardim.sendYardim(player);
		} else if (label.equalsIgnoreCase("b"))
		{
			new OOC(sender, label, args);
		} else if (label.equalsIgnoreCase("me"))
		{
			new Me(sender, label, args);
		} else if (label.equalsIgnoreCase("do"))
		{
			new Do(sender, label, args);
			// TODO: Had no clue what differenciated do and gdo
			// } else if (label.equalsIgnoreCase("gdo"))
			// {
			// Gdo.sendGdo(player, args);
		} else if (label.equalsIgnoreCase("dice"))
		{
			new Dice(sender, label, args);
		} else if (label.equalsIgnoreCase("w"))
		{
			new Whisper(sender, label, args);
		}
		return true;
	}

}
