package com.blackcowthrower.plugin;

import java.util.List;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;

public class Config
{
	public String character_prefix, channel_prefix, leveling_prefix;
	public String host, database, username, password;

	public int general_radius, shout_radius, low_radius, emote_radius,
			dice_radius, whisper_radius, join_leave_radius, ooc_radius, port;
	public int cooldown_chat, cooldown;

	public List<String> races;

	@SuppressWarnings("unchecked")
	public Config(Main configFile)
	{
		FileConfiguration con = configFile.getConfig();

		// Prefix Config
		character_prefix = con.getString("character_prefix").replaceAll(
				"(&([a-f0-9]))", "\u00A7$2");
		channel_prefix = con.getString("channel_prefix").replaceAll(
				"(&([a-f0-9]))", "\u00A7$2");
		leveling_prefix = con.getString("leveling_prefix").replaceAll(
				"(&([a-f0-9]))", "\u00A7$2");
		// End Prefix Config

		// MySQL Config
		host = con.getString("host");
		database = con.getString("database");
		port = con.getInt("port");
		username = con.getString("username");
		password = con.getString("password");
		// End MySQL Config

		// Cooldown Config
		cooldown_chat = con.getInt("cooldown_chat");
		cooldown = con.getInt("cooldown");
		// End Cooldown Config

		// Radius Config
		general_radius = con.getInt("general_radius");
		shout_radius = con.getInt("shout_radius");
		low_radius = con.getInt("low_radius");
		emote_radius = con.getInt("emote_radius");
		dice_radius = con.getInt("dice_radius");
		whisper_radius = con.getInt("whisper_radius");
		join_leave_radius = con.getInt("join_leave_radius");
		ooc_radius = con.getInt("ooc_radius");
		// End Radius Config

		// Races Config
		races = (List<String>) con.getList("Races");
		if (races == null)
			configFile.getLogger().log(Level.SEVERE, "Failed to load races");
		// End Races Config
	}
}
