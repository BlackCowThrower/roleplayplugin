package com.blackcowthrower.plugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;

public class Do extends CommandObject
{

	public Do(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;
		Player p = (Player) sender;

		if (args.length >= 1)
		{
			int eRadius = Main.getPluginConfig().emote_radius;
			StringBuilder sb = new StringBuilder();
			for (String arg : args)
			{
				sb.append(arg).append(" ");
			}
			String message = sb.toString().trim();

			if (eRadius != 0)
			{
				for (Entity entity : p.getNearbyEntities(eRadius, eRadius, eRadius))
				{
					if (entity instanceof Player)
					{
						Player nearby = (Player) entity;
						nearby.sendMessage(new StringBuilder().append(ChatColor.BLACK).append("[")
								.append(ChatColor.GOLD).append("!").append(ChatColor.BLACK)
								.append("]").append(ChatColor.GREEN).append(message).append(" ((")
								.append(p.getDisplayName()).append("))").toString());
					}
				}
				p.sendMessage(new StringBuilder().append(ChatColor.BLACK).append("[")
						.append(ChatColor.GOLD).append("!").append(ChatColor.BLACK).append("]")
						.append(ChatColor.GREEN).append(message).append(" ((")
						.append(p.getDisplayName()).append("))").toString());
			} else
			{
				Bukkit.broadcastMessage(new StringBuilder().append(ChatColor.BLACK).append("[")
						.append(ChatColor.GOLD).append("!").append(ChatColor.BLACK).append("]")
						.append(ChatColor.GREEN).append(message).append(" ((")
						.append(p.getDisplayName()).append("))").toString());
			}
		} else
		{
			CommandHelp(sender);
		}
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED).append("/do <Action>")
				.toString());
	}

}
