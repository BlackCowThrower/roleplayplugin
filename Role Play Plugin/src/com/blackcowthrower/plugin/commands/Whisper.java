package com.blackcowthrower.plugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;

public class Whisper extends CommandObject
{

	public Whisper(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;
		Player p = (Player) sender;
		if (args.length >= 2)
		{
			int wRadius = Main.getPluginConfig().whisper_radius;
			StringBuilder sb = new StringBuilder();
			for (String arg : args)
			{
				sb.append(arg).append(" ");
			}
			String message = sb.toString().substring(args[0].length()).trim();

			for (Entity e : p.getNearbyEntities(wRadius, wRadius, wRadius))
			{
				if (e instanceof Player)
				{
					Player nearby = (Player) e;

					if (args[0].equalsIgnoreCase(nearby.getName()))
					{
						int wPickup = 0; // TODO: Add config.
											// Language.whisper_pickup;

						if (wPickup != 0)
						{
							String pMsg = new StringBuilder().append(ChatColor.DARK_PURPLE)
									.append("* ").append(p.getDisplayName()).append(", ")
									.append(nearby.getDisplayName())
									.append(" whispers something in the ear").toString();
							for (Entity entity : p.getNearbyEntities(wPickup, wPickup, wPickup))
							{
								if (entity instanceof Player)
								{
									Player receiver = (Player) entity;
									receiver.sendMessage(pMsg);
								}
							}
							p.sendMessage(pMsg);
						} else
						{
							Bukkit.broadcastMessage(new StringBuilder()
									.append(ChatColor.LIGHT_PURPLE).append(p.getDisplayName())
									.append(", ").append(nearby.getDisplayName())
									.append(" mumbles something in the ear").toString());
						}

						p.sendMessage(new StringBuilder().append(ChatColor.GRAY)
								.append(nearby.getDisplayName()).append(" to you whispered: ")
								.append(message).toString());
						nearby.sendMessage(new StringBuilder().append(ChatColor.GRAY)
								.append(p.getDisplayName()).append(" whispered: ").append(message)
								.toString());
						return;
					}
				}
			}
			sender.sendMessage(new StringBuilder().append(Main.getPrefix())
					.append("No player exists by that name").toString());
		} else
		{
			CommandHelp(sender);
		}
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED)
				.append("/w <Player> <Message>").toString());
	}
}
