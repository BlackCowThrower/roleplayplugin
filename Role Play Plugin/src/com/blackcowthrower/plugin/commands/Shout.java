package com.blackcowthrower.plugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;

public class Shout extends CommandObject
{

	public Shout(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;

		Player p = (Player) sender;

		if (args.length >= 1)
		{
			int sRadius = Main.getPluginConfig().shout_radius;
			StringBuilder sb = new StringBuilder();
			for (String arg : args)
			{
				sb.append(arg).append(" ");
			}
			String message = sb.toString().trim();
			if (sRadius != 0)
			{
				int found = 0;
				for (Entity entity : p.getNearbyEntities(sRadius, sRadius, sRadius))
				{
					if (entity instanceof Player)
					{
						Player nearby = (Player) entity;
						nearby.sendMessage(new StringBuilder().append(ChatColor.GRAY)
								.append(p.getDisplayName()).append(" shouts: ")
								.append(ChatColor.WHITE).append(message).append("!").toString());
						found++;
					}
				}
				p.sendMessage(new StringBuilder().append(ChatColor.GRAY).append(p.getDisplayName())
						.append(" shouts: ").append(ChatColor.WHITE).append(message).append("!")
						.toString());

				if (found == 0)
					p.sendMessage(new StringBuilder().append(ChatColor.GRAY).append("Your message fell upon deaf ears.").toString());
			} else
			{
				p.chat(message + "!");
			}
		} else
		{
			CommandHelp(sender);
		}
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED).append("/s <Message>")
				.toString());
	}
}
