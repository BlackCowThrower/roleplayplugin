package com.blackcowthrower.plugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;

public class Low extends CommandObject
{

	public Low(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;

		Player p = (Player) sender;

		if (args.length >= 1)
		{
			int lRadius = Main.getPluginConfig().low_radius;
			StringBuilder sb = new StringBuilder();
			for (String arg : args)
			{
				sb.append(arg).append(" ");
			}
			String message = sb.toString().trim();
			if (lRadius != 0)
			{
				for (Entity entity : p.getNearbyEntities(lRadius, lRadius, lRadius))
				{
					if (entity instanceof Player)
					{
						Player nearby = (Player) entity;
						nearby.sendMessage(new StringBuilder().append(ChatColor.GRAY)
								.append(p.getDisplayName()).append(" mutters: ").append(message)
								.toString());
					}
				}
				p.sendMessage(new StringBuilder().append(ChatColor.GRAY).append(p.getDisplayName())
						.append(" mutters: ").append(message).toString());
			} else
			{
				p.chat(new StringBuilder().append(ChatColor.GRAY).append(message).toString());
			}
		} else
		{
			CommandHelp(sender);
		}
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED).append("/l <Message>")
				.toString());
	}
}
