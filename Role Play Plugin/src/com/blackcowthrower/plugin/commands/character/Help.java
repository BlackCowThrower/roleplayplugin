package com.blackcowthrower.plugin.commands.character;

import org.bukkit.command.CommandSender;

import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Help extends CommandObject
{

	public Help(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		sender.sendMessage(new StringBuilder().append(Main.getPrefix())
				.toString());
		sender.sendMessage(new StringBuilder().append(
				"/character name <First name> <Last name>").toString());
		sender.sendMessage(new StringBuilder().append("/character age <Age>")
				.toString());
		sender.sendMessage(new StringBuilder().append("/character race <Race>")
				.toString());
		sender.sendMessage(new StringBuilder().append(
				"/character bio [Any info for your character]:[Delete]")
				.toString());
		sender.sendMessage(new StringBuilder().append(
				"/character bio2 [A second line of info]:[Delete]").toString());
		sender.sendMessage(new StringBuilder().append("/character confirm")
				.toString());
		sender.sendMessage(new StringBuilder().append(
				"/character delete [Player]").toString());
	}

}
