package com.blackcowthrower.plugin.commands.character;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Bio extends CommandObject
{
	private String bio;

	public Bio(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			if (args.length >= 2)
			{
				if (args[1].equalsIgnoreCase("delete"))
				{
					List<Card> cards = Main.getCardList();
					for (Card card : cards)
					{
						if (card.getUUID().equals(p.getUniqueId()))
						{
							card.setBio(null);
							card.setBio2(null);
							sender.sendMessage(new StringBuilder().append(Main.getPrefix())
									.append(ChatColor.GREEN).append("Bio(s) cleared").toString());
							break;
						}
					}
					sender.sendMessage(new StringBuilder().append(Main.getPrefix())
							.append(ChatColor.RED).append("Card not found").toString());
				} else
				{

					StringBuilder sb = new StringBuilder();
					for (String arg : args)
					{
						sb.append(arg).append(" ");
					}

					String LongBio = sb.toString().substring(args[0].length());
					bio = LongBio.trim();
					// Character.hasSetBio.put(player, bio);

					if (!Main.getMySQLConnection().playerDataContainsPlayer(p.getUniqueId()))
					{
						if (bio.length() > 20)
							sender.sendMessage(new StringBuilder().append(Main.getPrefix())
									.append("Bio set!").append("(\"").append(bio.substring(0, 20))
									.append("...\"").toString());
						else
							sender.sendMessage(new StringBuilder().append(Main.getPrefix())
									.append("Bio set!").append(" (\"").append(bio).append("\")")
									.toString());
						// sender.sendMessage(Character.prefix +
						// Language.bio2_message);
					} else
					{
						// Kaydet.updateBio(player);
						sender.sendMessage(new StringBuilder().append(Main.getPrefix())
								.append("Bio set and updated!").toString());
					}
					// if (Character.hasSetAll(player))
					// player.sendMessage(Character.prefix +
					// Language.finish_card);
				}
			} else
			{
				CommandHelp(sender);
			}
		}
	}

	public String getBio()
	{
		return bio;
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED)
				.append("/Chracter bio <Bio Info>").toString());
	}
}
