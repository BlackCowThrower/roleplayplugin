package com.blackcowthrower.plugin.commands.character;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Age extends CommandObject
{
	private int age;

	public Age(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (args.length == 2)
		{
			try
			{
				age = Integer.parseInt(args[1]);
				sender.sendMessage(new StringBuilder().append(Main.getPrefix()).append("Age set!")
						.toString());
			} catch (Exception e)
			{
				sender.sendMessage(new StringBuilder().append(Main.getPrefix())
						.append(ChatColor.RED).append("Invalid Age").toString());
			}
		} else
		{
			CommandHelp(sender);
		}
	}

	public int getAge()
	{
		return age;
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		sender.sendMessage(new StringBuilder().append(ChatColor.RED).append("/character age <Age>").toString());
		super.CommandHelp(sender);
	}
}
