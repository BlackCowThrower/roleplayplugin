package com.blackcowthrower.plugin.commands.character;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Delete extends CommandObject
{
	Player p;

	@SuppressWarnings("deprecation")
	public Delete(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (sender instanceof Player)
		{
			p = (Player) sender;
			if (args.length == 2)
			{
				if (p.hasPermission("character.delete"))
				{
					for (Player players : Bukkit.getOnlinePlayers())
					{
						if (args[1].equalsIgnoreCase(players.getName()))
						{
							if (Main.getMySQLConnection().playerDataContainsPlayer(
									players.getUniqueId()))
							{
								Main.removeCardFromList(Main.getCard(players.getUniqueId()));
								Main.getMySQLConnection().deleteSQLCard(players.getUniqueId());
								
								p.sendMessage(new StringBuilder().append(Main.getPrefix())
										.append(ChatColor.GREEN).append("Card delted!").toString());
								p.teleport(p.getWorld().getSpawnLocation());
								p.setDisplayName(p.getName());
								return;
								// Character.allowed.add(p.getUniqueId());
							} else
							{
								p.sendMessage(new StringBuilder().append(Main.getPrefix())
										.append(ChatColor.RED).append("No card found").toString());
							}
						}
					}
					p.sendMessage(new StringBuilder().append(Main.getPrefix())
							.append(ChatColor.RED).append("No player exists").toString());
				} else
				{
					p.sendMessage(new StringBuilder().append(Main.getPrefix())
							.append(ChatColor.RED).append("Permission denied").toString());
				}
			} else
			{
				CommandHelp(sender);
			}
		}
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		if (p.hasPermission("character.delete"))
		{
			p.sendMessage(new StringBuilder().append("/Character delete [Player]").toString());
		} else
		{
			p.sendMessage(new StringBuilder().append(Main.getPrefix()).append(ChatColor.RED)
					.append("Permission denied").toString());
		}
	}

}
