package com.blackcowthrower.plugin.commands.character;

import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.MySQL;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Character extends CommandObject
{
	public Character(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			if (Main.getMySQLConnection().playerDataContainsPlayer(p.getUniqueId()))
			{
				for (Card card : Main.getCardList())
				{
					if (p.getUniqueId().equals(card.getUUID()))
					{
						Card.displayCard(p, card);
						return;
					}
				}
				sender.sendMessage(new StringBuilder().append(Main.getPrefix())
						.append(ChatColor.RED).append("No card found").toString());
			} else
			{
				sender.sendMessage(new StringBuilder().append(Main.getPrefix())
						.append(ChatColor.RED).append("No card found").toString());
			}
		}
	}

	protected static boolean checkName(CommandSender sender, String name)
	{
		MySQL mysql = Main.getMySQLConnection();
		List<Object> names = mysql.getColumn("RP_Name");
		for (Object s : names)
		{
			if (name.equalsIgnoreCase((String) s))
			{
				sender.sendMessage(new StringBuilder().append(Main.getPrefix())
						.append(ChatColor.RED).append("Name already exists!").toString());
				return false;
			}
		}
		return true;
	}

	public static void saveData(Card card)
	{
		MySQL mysql = Main.getMySQLConnection();
		if (!mysql.playerDataContainsPlayer(card.getUUID()))
		{
			mysql.createSQLCard(card);
		} else
		{
			UUID uuid = card.getUUID();
			if (!card.getRPName().isEmpty())
				mysql.updateObject(uuid, "RP_Name", card.getRPName());
			if (card.getAge() >= 0)
				mysql.updateObject(uuid, "Age", card.getAge());
			if (!card.getRace().toString().isEmpty())
				mysql.updateObject(uuid, "Race", card.getRace().toString());
			mysql.updateObject(uuid, "Bio", card.getBio());
			mysql.updateObject(uuid, "Bio2", card.getBio2());
			mysql.updateObject(uuid, "Level", card.getLevel());
		}
	}
}
