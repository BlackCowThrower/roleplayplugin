package com.blackcowthrower.plugin.commands.character;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.TempCard;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Confirm extends CommandObject
{

	public Confirm(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		Player player = (Player) sender;
		UUID uuid = player.getUniqueId();

		TempCard tc = TempCard.checkForCard(uuid);
		if (tc != null)
		{
			Card c = tc.finalise();
			if(c != null)
			{
				Main.addCardToList(c);
				Main.getMySQLConnection().createSQLCard(c);
				player.sendMessage(new StringBuilder()
				.append(Main.getPrefix())
				.append(ChatColor.GREEN)
				.append("Welcome to the server, ")
				.append(ChatColor.GOLD)
				.append(c.getRPName())
				.toString());
				//TODO: Set display name
				//TOOD: Set tab&tag name
				//TODO: Possibly broadcast
			}else
			{
				player.sendMessage(new StringBuilder()
				.append(Main.getPrefix())
				.append(ChatColor.RED)
				.append("Your character is not finished!. Please refer to /Character help")
				.toString());
				return;
			}
		} else
		{
			player.sendMessage(new StringBuilder()
					.append(Main.getPrefix())
					.append(ChatColor.RED)
					.append("To create a character please refer to /Character help")
					.toString());
			return;
		}
	}
}
