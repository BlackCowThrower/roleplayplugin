package com.blackcowthrower.plugin.commands.character;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Name extends CommandObject
{
	private String name;

	public Name(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (args.length >= 3)
		{
			if (args[1].contains("_") || args[2].contains("_") || args[1].equals("")
					|| args[2].equals(""))
			{
				sender.sendMessage(new StringBuilder().append(Main.getPrefix())
						.append(ChatColor.RED).append("Invalid Name").toString());
			} else
			{
				String nameInput = args[1] + "_" + args[2];
				if (Character.checkName(sender, nameInput))
				{
					if (nameInput.length() <= 16)
					{
						name = nameInput;
						sender.sendMessage(new StringBuilder().append(Main.getPrefix())
								.append("Name set!").toString());
					} else
					{
						sender.sendMessage(new StringBuilder().append(Main.getPrefix())
								.append(ChatColor.RED)
								.append("Name cannot be more than 15 characters").toString());
					}
				}
			}
		} else
		{
			CommandHelp(sender);
		}
	}

	public String getName()
	{
		return name;
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		sender.sendMessage(new StringBuilder().append(ChatColor.RED)
				.append("/Character name <FirstName> <SecondName>").toString());
		super.CommandHelp(sender);
	}
}
