package com.blackcowthrower.plugin.commands.character;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Race extends CommandObject
{
	private String race;
	public Race(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (args.length == 2)
		{
			for (String r : Main.getPluginConfig().races)
			{
				if (args[1].equalsIgnoreCase(r.toString()))
				{
					race = r;
					sender.sendMessage(new StringBuilder().append(Main.getPrefix())
							.append("Race set!").toString());
					return;
				}
			}
			sender.sendMessage(new StringBuilder().append(Main.getPrefix()).append(ChatColor.RED)
					.append("Race not found!").toString());
		} else
		{
			CommandHelp(sender);
		}
	}
	
	public String getRace()
	{
		return race;
	}
	
	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED)
				.append("/Chracter race <Race>").toString());
	}
}
