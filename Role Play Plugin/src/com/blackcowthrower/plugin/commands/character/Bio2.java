package com.blackcowthrower.plugin.commands.character;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Main;
import com.blackcowthrower.plugin.commands.CommandObject;

public class Bio2 extends CommandObject	
{
	private static String bio2;
	
	public Bio2(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			if (args.length >= 2)
			{
				if (args[1].equalsIgnoreCase("delete"))
				{
					List<Card> cards = Main.getCardList();
					for (Card card : cards)
					{
						if (card.getUUID().equals(p.getUniqueId()))
						{
							card.setBio2(null);
							sender.sendMessage(new StringBuilder().append(Main.getPrefix())
									.append(ChatColor.GREEN).append("Bio2 cleared").toString());
							break;
						}
					}
					sender.sendMessage(new StringBuilder().append(Main.getPrefix())
							.append(ChatColor.RED).append("Card not found").toString());
				} else
				{

					StringBuilder sb = new StringBuilder();
					for (String arg : args)
					{
						sb.append(arg).append(" ");
					}

					String LongBio = sb.toString().substring(args[0].length());
					bio2 = LongBio.trim();

					if (!Main.getMySQLConnection().playerDataContainsPlayer(p.getUniqueId()))
					{
						if (bio2.length() > 20)
							sender.sendMessage(new StringBuilder().append(Main.getPrefix())
									.append("Bio2 set!").append("(\"").append(bio2.substring(0, 20))
									.append("...\"").toString());
						else
							sender.sendMessage(new StringBuilder().append(Main.getPrefix())
									.append("Bio2 set!").append(" (\"").append(bio2).append("\")")
									.toString());
					} else
					{
						sender.sendMessage(new StringBuilder().append(Main.getPrefix())
								.append("Bio2 set and updated!").toString());
					}
				}
			} else
			{
				CommandHelp(sender);
			}
		}
	}
	
	public String getBio2()
	{
		return bio2;
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED)
				.append("/Chracter bio2 <Bio Info>").toString());
	}
}	
