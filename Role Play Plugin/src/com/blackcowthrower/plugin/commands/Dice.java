package com.blackcowthrower.plugin.commands;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;

public class Dice extends CommandObject
{

	public Dice(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;
		Player p = (Player) sender;
		int zRadius = Main.getPluginConfig().dice_radius;
		Random rand = new Random();
		int randomNum = rand.nextInt(6) + 1;

		if (zRadius != 0)
		{
			for (Entity entity : p.getNearbyEntities(zRadius, zRadius, zRadius))
			{
				if (entity instanceof Player)
				{
					Player nearby = (Player) entity;
					nearby.sendMessage(new StringBuilder().append(ChatColor.BLACK).append("[")
							.append(ChatColor.GOLD).append("!").append(ChatColor.BLACK).append("]")
							.append(ChatColor.GREEN).append("Threw dice: ").append(randomNum)
							.append(" ((").append(p.getDisplayName()).append("))").toString());
				}
			}
			p.sendMessage(new StringBuilder().append(ChatColor.BLACK).append("[")
					.append(ChatColor.GOLD).append("!").append(ChatColor.BLACK).append("]")
					.append(ChatColor.GREEN).append("Threw dice: ").append(randomNum).append(" ((")
					.append(p.getDisplayName()).append("))").toString());
		} else
		{
			Bukkit.broadcastMessage(new StringBuilder().append(ChatColor.BLACK).append("[")
					.append(ChatColor.GOLD).append("!").append(ChatColor.BLACK).append("]")
					.append(ChatColor.GREEN).append("Threw dice: ").append(randomNum).append(" ((")
					.append(p.getDisplayName()).append("))").toString());
		}
	}
}
