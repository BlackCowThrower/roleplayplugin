package com.blackcowthrower.plugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Card;
import com.blackcowthrower.plugin.Main;

public class ID extends CommandObject
{

	@SuppressWarnings("deprecation")
	public ID(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;

		Player p = (Player) sender;

		if (!p.hasPermission("character.lookup"))
		{
			p.sendMessage(new StringBuilder().append(Main.getPrefix()).append(ChatColor.RED)
					.append("Permission Denied").toString());
			return;
		}

		for (Player players : Bukkit.getOnlinePlayers())
		{
			String msg = new StringBuilder().append(Main.getPrefix()).append(ChatColor.BLUE)
					.append("MC Username ").append("%mc").append(" : ")
					.append("%rp").toString();
			if (args.length == 1)
			{
				if (args[0].equalsIgnoreCase(players.getName()))
				{
					if (Main.getMySQLConnection().playerDataContainsPlayer(players.getUniqueId()))
					{
						Card c = Main.getCard(players.getUniqueId());
						p.sendMessage(msg.replaceAll("%mc", players.getName()).replaceAll("%rp", c.getRPName()));
					} else
						p.sendMessage(new StringBuilder().append(Main.getPrefix())
								.append(players.getName()).append(" has no card")
								.toString());
					return;
				}
			} else if (args.length == 2)
			{
				StringBuilder sb = new StringBuilder().append(args[0]).append(" ").append(args[1]);
				String name = sb.toString();
				if (name.equalsIgnoreCase(players.getDisplayName()))
				{
					if (Main.getMySQLConnection().playerDataContainsPlayer(players.getUniqueId()))
					{
						Card c = Main.getCard(players.getUniqueId());
						p.sendMessage(msg.replaceAll("%mc", players.getName()).replaceAll("%rp", c.getRPName()));
						return;
					} else
						p.sendMessage(new StringBuilder().append(Main.getPrefix())
								.append(players.getName()).append(" has no card")
								.toString());
				}
			} else
			{
				CommandHelp(sender);
				return;
			}
		}
		p.sendMessage(new StringBuilder().append(Main.getPrefix()).append(ChatColor.RED)
				.append("Player does not exist").toString());
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED).append("/ID <Plyer:Name>")
				.toString());
	}
}
