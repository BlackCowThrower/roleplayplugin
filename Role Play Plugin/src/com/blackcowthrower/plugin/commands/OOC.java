package com.blackcowthrower.plugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.blackcowthrower.plugin.Main;

public class OOC extends CommandObject
{

	public OOC(CommandSender sender, String command, String[] args)
	{
		super(sender, command, args);
		if (!(sender instanceof Player))
			return;

		Player p = (Player) sender;

		if (args.length > 0)
		{
			int lRadius = Main.getPluginConfig().ooc_radius;
			StringBuilder sb = new StringBuilder();
			for (String arg : args)
			{
				sb.append(arg).append(" ");
			}
			String message = sb.toString().trim();
			if (lRadius != 0)
			{
				String msg = new StringBuilder().append(ChatColor.GRAY).append("(([OOC]")
						.append(p.getDisplayName()).append(": ").append(message).append("))")
						.toString();
				for (Entity entity : p.getNearbyEntities(lRadius, lRadius, lRadius))
				{
					if (entity instanceof Player)
					{
						Player nearby = (Player) entity;
						nearby.sendMessage(msg);
					}
				}
				p.sendMessage(msg);
			}
		} else
		{
			CommandHelp(sender);
		}
	}

	@Override
	public void CommandHelp(CommandSender sender)
	{
		super.CommandHelp(sender);
		sender.sendMessage(new StringBuilder().append(ChatColor.RED).append("/b <Message>")
				.toString());
	}
}
